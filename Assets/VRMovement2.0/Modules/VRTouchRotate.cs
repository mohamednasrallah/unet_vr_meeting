﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class VRTouchRotate : MonoBehaviour {

    public bool canRotate = true;
    public enum eRotationMode { PointAndShoot, QuickStick, SlowStick, None };
    [Header("-Rotate Modes-")]
    public eRotationMode RotationMode = eRotationMode.QuickStick;
    [Header("-Rotate Controls-")]
    public OVRInput.Button PointAndShootRotateButton;      //Rotate to Where Controller Is Located
    [Header("-Rotate Settings-")]
    public bool fadeRotate = false;
    public float rotateTime = .2f;            // ButtonRotation
    public float slowRotateSpeed = .7f;           //Speed for Stick Rotate
    VRTouchMove2 refSystem;
    void Start()
    {
        refSystem = GetComponent<VRTouchMove2>();
    }

void Update()
    {
        if(canRotate)
        {
            if (RotationMode == eRotationMode.PointAndShoot)
            {
                PointShootPressed();
            }
            if (RotationMode == eRotationMode.QuickStick)
            {
                QuickStickRotate();
            }
            if (RotationMode == eRotationMode.SlowStick)
            {
                SlowStickRotate();
            }
        }
    }
    //Slowly Rotates the Charactor Controller
    void SlowStickRotate()
    {
        float h = 0;
        switch (refSystem.ControlsOn)
        {
            case VRTouchMove2.eControllerType.Left:
                h = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch).x;
                break;
            case VRTouchMove2.eControllerType.Right:
                h = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).x;
                break;
            case VRTouchMove2.eControllerType.Both:
                h = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch).x;
                h += OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).x;
                h = Mathf.Clamp(h, -1, 1);
                break;

        }
        if (Mathf.Abs(h) > .1f)
        {
            RotateByDegrees(h * slowRotateSpeed);
        }
    }
    //Quick 45 Degree Rotations for the Charactor Controller
    void QuickStickRotate()
    {
        switch (refSystem.ControlsOn)
        {
            case VRTouchMove2.eControllerType.Left:
                if (OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.LTouch))
                {
                    RotateByDegrees(-45);
                }
                if (OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.LTouch))
                {
                    RotateByDegrees(45);
                }
                break;
            case VRTouchMove2.eControllerType.Right:
                if (OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.RTouch))
                {
                    RotateByDegrees(-45);
                }
                if (OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.RTouch))
                {
                    RotateByDegrees(45);
                }
                break;
            case VRTouchMove2.eControllerType.Both:
                if (OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.LTouch))
                {
                    RotateByDegrees(-45);
                }
                if (OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.LTouch))
                {
                    RotateByDegrees(45);
                }
                if (OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.RTouch))
                {
                    RotateByDegrees(-45);
                }
                if (OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.RTouch))
                {
                    RotateByDegrees(45);
                }
                break;

        }
    }
    //Point Shoot Pressed Event
    void PointShootPressed()
    {
        VRTouchMove2.InputData InputHolder = refSystem.InputReturnDown(PointAndShootRotateButton);
       if(InputHolder.isRight == true)
        {
            PointAndShootRotation(refSystem.rightController);
        }
        if (InputHolder.isLeft == true)
        {
            PointAndShootRotation(refSystem.leftController);
        }
    }

    /// <summary>
    /// Point and Shoot Rotation ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ///  Changes the Rotation based on where the Controller is pointing and where you are looking only only in one direction. This allows someone to quickly look behind them.
    /// </summary>

    void PointAndShootRotation(Transform selectedController)
    {
            if (fadeRotate)
            {
                refSystem.myFade.StartFadeIn(refSystem.fadeTime);
            }
            //Get Position in front of Object
            Vector3 holder = new Ray(selectedController.position, selectedController.forward).GetPoint(1);
            //Get Rotational Direction
            Vector3 lookPos = holder - refSystem.headRig.transform.position;
            //Remove Y Component
            lookPos.y = 0;
            //Transform rotation
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            //Get Rotational Direction
            Vector3 rotPosition = rotation.eulerAngles - refSystem.transform.localRotation.eulerAngles;
            //Flatten Rotational Return
            rotPosition.x = 0;
            rotPosition.z = 0;
            //Apply Rotation
            refSystem.yourRig.transform.DORotate(rotPosition, rotateTime);
    }

    //Helper Function To rotate by set Degrees
    public void RotateByDegrees(float degrees)
    {
        if (refSystem.myFade && fadeRotate)
        {
            refSystem.myFade.StartFadeIn(refSystem.fadeTime);
        }
        Vector3 holder1;
        holder1 = refSystem.yourRig.transform.rotation.eulerAngles;
        holder1.y += degrees;
        Vector3 rotPosition = holder1;
        refSystem.yourRig.transform.DORotate(rotPosition, rotateTime);
    }
}

