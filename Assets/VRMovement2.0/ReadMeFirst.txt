/*******************************************************
VR Movement System for Oculus Touch 

Copyright (C) 2016 3lbGames

This Update requires you to Delete the entire VR Package

VRTouchMove can not be copied and/or distributed without the express permission of 3lbGames - Chris Castaldi.

For additional Information or unity development services please contact services@3lbgames.com

For technical support contact support@3lbgames.com

 *******************************************************/

 Please Check out the provided documentation and Demo scenes if this is your first time using it.

 For the latest documentation please visit: https://docs.google.com/document/d/1CeMn1Q9xN6ERB8i6L3USLZACg3-vwqaM7mtey5lgYPA/edit?usp=sharing

#################-IF YOU HAVE COMPLIER ERRORS-##################

If after Import you have compiler errors do these steps first. If you are still having trouble please contact support. support@3lbgames.com

1) OVR UtIlities is Imported:
https://developer.oculus.com/downloads/package/oculus-utilities-for-unity-5/1.3.2/

2) DOTween is correctly imported:
https://www.assetstore.unity3d.com/en/#!/content/27676

#################-IF YOU HAVE COMPLIER ERRORS-##################

 Quick Start

1) Drag out the OVRCameraRig.
2) Apply a Character Controller to the OVRCameraRig and size it as you see fit.
3) Drag out the VRMovementBase Prefab.
4) Pick which hand you want to control the movement system with you can chose left right or both.


Press Play, and you are ready for AutoFlight!

You can experiment by adding modules. For more information about these, look at the module section in the documentation.
