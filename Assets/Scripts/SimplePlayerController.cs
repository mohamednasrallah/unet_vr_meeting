﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimplePlayerController : NetworkBehaviour
{
    public float speed;
    public float rSpeed;

    void FixedUpdate ()
    {
		if(!isLocalPlayer)
        {
            return;
        }

        else
        {
            Movement(transform);
        }   
    }

    void Movement(Transform whichOne)
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.Q))
        {
            whichOne.Rotate(Vector3.down * rSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.E))
        {
            whichOne.Rotate(Vector3.up * rSpeed * Time.deltaTime);
        }
        whichOne.Translate(new Vector3(h, 0, v) * Time.deltaTime * speed);
    }
}
