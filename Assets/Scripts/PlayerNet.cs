﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerNet : NetworkBehaviour
{

    public GameObject OVRRig;
    public GameObject DummyRig;
    public Transform MenuPosition;
    //GameObject ArmMenu;
    // Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
        {
            OVRRig.SetActive(true);
            DummyRig.SetActive(true);
            //ArmMenu = GameObject.FindGameObjectWithTag("MovableUI");
        }
        else
        {
            OVRRig.GetComponent<OVRManager>().enabled = false;
            OVRRig.GetComponent<OVRCameraRig>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
