﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class NetworkMessageTestString : NetworkBehaviour
{
    //Each of these need to be different start at 1002
    const short ColorChangeMsg = 1002;
    const short StringChangeMsg = 1003;
    [SyncVar(hook = "HookIntChanged")]
    public int colorIndex;
    [SyncVar(hook = "HookStringChanged")]
    public string textToDisplay;
    NetworkClient m_client;
    public Text myText;

    public void Start()
    {
        //Assign NetworkMessages 
        Init(NetworkManager.singleton.client);
    }
    //Start Up Message System
    public void Init(NetworkClient client)
    {
        //Set Client | The NetworkClient is stored on this singleton in the NetworkManager
        m_client = client;
        //Only the Server Registers the Message
        if (isServer)
        {
            //Only the Server Registers the Message
            NetworkServer.RegisterHandler(ColorChangeMsg, ReceiveColorChangeMsg);
            NetworkServer.RegisterHandler(StringChangeMsg, ReceiveStringChangeMsg);
        }
    }

    public void BeginColorMessage(int myId)
    {
        //Change if SyncVar is 
        if (isServer)
        {
            colorIndex = myId;
        }
        else
        {
            //Create And Send Message
            IntegerMessage msg = new IntegerMessage(myId);
            m_client.Send(ColorChangeMsg, msg);
        }

    }

    public void BeginStringMessage(string myString)
    {
        //Change if SyncVar is 
        if (isServer)
        {
            textToDisplay = myString;
        }
        else
        {
            //Create And Send Message
            StringMessage msg = new StringMessage(myString);
            m_client.Send(StringChangeMsg, msg);
        }
    }

    //Receive The Color Change Message
    void ReceiveColorChangeMsg(NetworkMessage netMsg)
    {
        //Read the Int Message
        IntegerMessage curMessage = netMsg.ReadMessage<IntegerMessage>();
        //Set Value to Int Message
        colorIndex = curMessage.value;
    }

    //Receive The Color Change Message
    void ReceiveStringChangeMsg(NetworkMessage netMsg)
    {
        //Read the Int Message
        StringMessage curMessage = netMsg.ReadMessage<StringMessage>();
        //Set Value to Int Message
        textToDisplay = curMessage.value;
    }

    //Update Value on Hook
    public void HookIntChanged(int index)
    {
        ColorChanger.instance.ChangeColor(index);
    }

    //Update Value on Hook
    public void HookStringChanged(string index)
    {
        myText.text = index;
    }

}