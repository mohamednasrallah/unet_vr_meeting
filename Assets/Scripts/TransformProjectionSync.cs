﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 0.01f)]
public class TransformProjectionSync : NetworkBehaviour
{

    public Transform Source;
    public Transform Dummy;

    [SyncVar(hook = "SyncPosition")]
    public Vector3 syncTransformPos;

    [SyncVar(hook = "SyncRotation")]
    public Quaternion syncTransformRot;

    public bool inDummyMode;

    private void Start()
    {
        if (isLocalPlayer)
        {
            name = "LocalPlayer";
        }
        else
        {
            name = "Clone";
        }
    }
    void Update()
    {
        // This happens only on client and only if this is a local player
        if (isLocalPlayer)
        {
            // Move Source Transform
            // Source.position += Vector3.up * Input.GetAxis("Vertical") * Time.deltaTime * 20;

            // Update Dummy Transform to match Source Transform
            Dummy.position = Source.position;
            Dummy.rotation = Source.rotation;

            // Send the Dummy Transform position and rotation to the server
            CmdTransmitTransform(Dummy.position, Dummy.rotation);
        }
        else
        {
            inDummyMode = true;
        }
    }

    [Command]
    void CmdTransmitTransform(Vector3 position, Quaternion rotation)
    {
        // This is executed on the server
        // The client has sent us the new postion and rotation

        // Update the Dummy Transform with the values given to us by the client
        Dummy.position = position;
        Dummy.rotation = rotation;

        // Update the syncvars to match the new Dummy Transform
        syncTransformPos = position;
        syncTransformRot = rotation;
    }

    [Client]
    void SyncPosition(Vector3 latestPos)
    {
        // This is executed on the client and only if this object belongs to a different player
        if (!isLocalPlayer)
            Dummy.position = latestPos;
    }

    [Client]
    void SyncRotation(Quaternion latestRot)
    {
        // This is executed on the client and only if this object belongs to a different player
        if (!isLocalPlayer)
            Dummy.rotation = latestRot;
    }

}