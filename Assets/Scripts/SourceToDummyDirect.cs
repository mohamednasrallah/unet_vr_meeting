﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SourceToDummyDirect : NetworkBehaviour {

    public GameObject SourceHead;
    public GameObject SourceLeft;
    public GameObject SourceRight;

    public GameObject DummyHead;
    public GameObject DummyLeft;
    public GameObject DummyRight;

    public GameObject OVR;

    public Transform UICenterPoint;
    public GameObject yourUI;
    public SimplfiedMenuSystem myMenuSystem;
    public GameObject dualInput;
    // Update is called once per frame

    private void Start()
    {
        if (isLocalPlayer)
        {
            name = "LocalPlayer";
            OVR.SetActive(true);
            dualInput.SetActive(true);
            MoveUI();
        }
    }
    void Update ()
    {
        if (isLocalPlayer)
        {
            TranslateToDummy();

        }
        else
        {

        }
    }

    public void MoveUI()
    {
        yourUI = NetFinderGameObject.instance.theUI;
        yourUI.SetActive(true);
        yourUI.transform.position = UICenterPoint.position;
        yourUI.transform.parent = UICenterPoint;

        yourUI.transform.localScale = UICenterPoint.localScale / 10;
        Invoke("SetDelayShort", .2f);
    }

    void SetDelayShort()
    {
        myMenuSystem.GetUISystem(yourUI);
    }

    public void TranslateToDummy()
    {
        //Position
        DummyHead.transform.position = SourceHead.transform.position;
        DummyLeft.transform.position = SourceLeft.transform.position;
        DummyRight.transform.position = SourceRight.transform.position;

        //Rotation
        DummyHead.transform.rotation = SourceHead.transform.rotation;
        DummyLeft.transform.rotation = SourceLeft.transform.rotation;
        DummyRight.transform.rotation = SourceRight.transform.rotation;
    }
}
