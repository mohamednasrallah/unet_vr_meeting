﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RimChanger : MonoBehaviour
{
    public int rimCacheInt;
    public static RimChanger instance;
    public GameObject[] currentRims;

    void Awake()
    {
        instance = this;
    }

    public void ChangeRim(int selectedRim)
    {
        rimCacheInt = selectedRim;

        for (int i = 0; i < currentRims.Length; i++)
        {
            if(currentRims[i].activeInHierarchy)
            {
                currentRims[i].SetActive(false);
            }
        }
        currentRims[rimCacheInt].SetActive(true);
    }


}
