﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
//using UnityEngine.UI;

public class NetworkMessageTestB : NetworkBehaviour
{
    //Each of these need to be different start at 1002
    const short RimChangeMsg = 1003;
    [SyncVar(hook = "HookIntChanged")]
    public int rimIndex;
    NetworkClient m_client;

    public void Start()
    {
        //Assign NetworkMessages 
        Init(NetworkManager.singleton.client);
    }
    //Start Up Message System
    public void Init(NetworkClient client)
    {
        //Set Client | The NetworkClient is stored on this singleton in the NetworkManager
        m_client = client;
        //Only the Server Registers the Message
        if (isServer)
        {
            //Only the Server Registers the Message
            NetworkServer.RegisterHandler(RimChangeMsg, ReceiveRimChangeMsg);
        }
    }

    //
    public void BeginRimMessage(int myId)
    {
        //Change if SyncVar is 
        if (isServer)
        {
            rimIndex = myId;
        }
        else
        {
            //Create And Send Message
            IntegerMessage msg = new IntegerMessage(myId);
            m_client.Send(RimChangeMsg, msg);
        }

    }

    //Receive The Rim Change Message
    void ReceiveRimChangeMsg(NetworkMessage netMsg)
    {
        //Read the Int Message
        IntegerMessage curMessage = netMsg.ReadMessage<IntegerMessage>();
        //Set Value to Int Message
        rimIndex = curMessage.value;
    }

    //Update Value on Hook
    public void HookIntChanged(int index)
    {
        //The hook needs to change the value on a local non-networked monobehavior
        //Making it a singleton makes it easier to access.
        RimChanger.instance.ChangeRim(index);
    }

}