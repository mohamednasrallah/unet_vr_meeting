﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorChanger : MonoBehaviour
{
    public int colorCacheInt;
    public static ColorChanger instance;
    public GameObject Car;
    public Material[] colorList;

    void Awake()
    {
        instance = this;
    }

    public void ChangeColor(int selectedColor)
    {
        colorCacheInt = selectedColor;
        Car.GetComponent<Renderer>().material = colorList[colorCacheInt];
    }

}
