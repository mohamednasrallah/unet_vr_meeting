﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

[NetworkSettings(channel = 0, sendInterval = 0.01f)]
public class TransformProjectionSync_New : NetworkBehaviour
{
    [SyncVar(hook = "SyncPositionValues")]
    private Vector3 syncPos;

    private float lerpRate;
    public float normalLerpRate = 16;
    //private float fasterLerpRate = 27;

    private Vector3 lastPos;
    public float threshold = 0.5f;

    private List<Vector3> syncPosList = new List<Vector3>();
    [SerializeField] private bool useHistoricalInterpolation;

    private float closeEnough = 0.11f;

    [SyncVar(hook = "OnPlayerRotSynced")] private float syncPlayerRotation;

    [SerializeField] private Transform Source;
    [SerializeField] private Transform Dummy;

    private float lastPlayerRot;
    private float lastCamRot;

    private List<float> syncPlayerRotList = new List<float>();

    void Start()
    {
        lerpRate = normalLerpRate;
    }

    void Update()
    {
        LerpPosition();
        LerpRotations();
    }

    void FixedUpdate()
    {
        TransmitPosition();
        TransmitRotations();
    }

    void OrdinaryLerping()
    {
        LerpPlayerRotation(syncPlayerRotation);
        Dummy.position = Vector3.Lerp(Source.position, syncPos, Time.deltaTime * lerpRate);
    }

    void LerpPosition()
    {
        if (!isLocalPlayer)
        {
            OrdinaryLerping();
        }
    }


    void LerpRotations()
    {
        if (!isLocalPlayer)
        {
            if (useHistoricalInterpolation)
            {
                HistoricalInterpolation();
            }
            else
            {
                OrdinaryLerping();
            }
        }
    }

    void LerpPlayerRotation(float rotAngle)
    {
        Vector3 playerNewRot = new Vector3(0, rotAngle, 0);
        Dummy.rotation = Quaternion.Lerp(Source.rotation, Quaternion.Euler(playerNewRot), lerpRate * Time.deltaTime);
    }

    void HistoricalInterpolation()
    {
        if (syncPlayerRotList.Count > 0)
        {
            LerpPlayerRotation(syncPlayerRotList[0]);

            if (Mathf.Abs(Source.localEulerAngles.y - syncPlayerRotList[0]) < closeEnough)
            {
                syncPlayerRotList.RemoveAt(0);
            }
        }
    }


    [Command]
    void CmdProvideRotationsToServer(float playerRot)
    {
        syncPlayerRotation = playerRot;
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer && Vector3.Distance(Source.position, lastPos) > threshold)
        {
            CmdProvidePositionToServer(Source.position);
            lastPos = Source.position;
        }
    }

    [Command]
    void CmdProvidePositionToServer(Vector3 pos)
    {
        syncPos = pos;
    }

    bool CheckIfBeyondThreshold(float rot1, float rot2)
    {
        if (Mathf.Abs(rot1 - rot2) > threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [Client]
    void TransmitRotations()
    {
        if (isLocalPlayer)
        {
            if (CheckIfBeyondThreshold(Source.localEulerAngles.y, lastPlayerRot))
            {
                lastPlayerRot = Source.localEulerAngles.y;
                CmdProvideRotationsToServer(lastPlayerRot);
            }
        }
    }


    [Client]
    void SyncPositionValues(Vector3 latestPos)
    {
        syncPos = latestPos;
        syncPosList.Add(syncPos);
    }

    [Client]
    void OnPlayerRotSynced(float latestPlayerRot)
    {
        syncPlayerRotation = latestPlayerRot;
        syncPlayerRotList.Add(syncPlayerRotation);
    }


}