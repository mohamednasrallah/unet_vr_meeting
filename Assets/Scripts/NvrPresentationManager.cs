﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class NvrPresentationManager : MonoBehaviour
{ 
    bool colorButtonComplete = false;  
    public Material[] colorVarietyMaterials;        //these are the colors dragged from the project files into an array for the program to use

    public GameObject colorSphere;      //this is the color ball that will be instantiated with each color added
    Vector3 prevColorSpherePOS;     //cached pos of instantiated colorspheres to move them away from each other

    private void Start()
    {
        prevColorSpherePOS = colorSphere.transform.position;
        ColorSphereMaker();
    }

    private void ColorSphereMaker()
    {
        if (colorButtonComplete == true)
        {
            return;
        }

        if (colorButtonComplete == false)
        {

            for (int index = 0; index < colorVarietyMaterials.Length; index++)
            {
                GameObject colorSphereClone = Instantiate(colorSphere);
                colorSphereClone.GetComponent<Renderer>().material = colorVarietyMaterials[index];
                colorSphereClone.transform.position = prevColorSpherePOS;
                prevColorSpherePOS.x = colorSphereClone.transform.position.x + 1;

                colorButtonComplete = true;
            }
        }
    }

}

