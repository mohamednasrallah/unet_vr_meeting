﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class OnScreenNetworkStatus : NetworkBehaviour {

    private void Start()
    {
        changetext();
    }

    void changetext()
    {
        if (isClient && !isServer)
        {
            gameObject.GetComponent<Text>().text += "CLIENT";
        }

        if (isServer)
        {
            gameObject.GetComponent<Text>().text += "SERVER";
        }
    }
}
