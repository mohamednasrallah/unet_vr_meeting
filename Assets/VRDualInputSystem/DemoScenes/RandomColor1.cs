﻿using UnityEngine;
using System.Collections;

public class RandomColor1 : MonoBehaviour {

	public void ChangeColors()
    {
        GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value);
    }
}
