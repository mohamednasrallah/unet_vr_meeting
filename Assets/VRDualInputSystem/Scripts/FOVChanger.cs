﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FOVChanger : MonoBehaviour {
    public Camera myCamera;
    public float goTo = 100;
	// Use this for initialization
	void Start ()
    {
        myCamera.DOFieldOfView(goTo, .2f);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            myCamera.DOFieldOfView(goTo, .2f);
        }
	}
}
