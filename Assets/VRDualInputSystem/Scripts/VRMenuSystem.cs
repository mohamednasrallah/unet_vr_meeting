﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
public class VRMenuSystem : MonoBehaviour {

    [Header("-HookUps-")]
    public Transform yourEyes;
    public CanvasGroup vrUIGroup;
    [Header("-Settings-")]
    public bool oneTapOpens;
    public bool UIOn;
    public Vector3 Offset;
    public float distance;
    public bool doNotReposition;
    public float fadeTime = .5f;
    [Header("-OverRide Settings-")]
    public bool xOverRide;
    public float xOverRideNum;


    [Header("-DoubleTapInfo-")]
    public bool useDoubleTap;
    public float tapSpeed = .3f;

    float tapCount;
    float tapTime;
    [Header("-Image Swapper-")]
    public Image menuImage;
    public Sprite closeMenuSprite;
    public Sprite menuSprite;
    [Header("-Debug Keys-")]
    public bool useDebugKey;
    public KeyCode DebugKey;
    // Use this for initialization
    void Start ()
    {
        if(menuImage)
        {
            menuSprite = menuImage.sprite;
        }
        vrUIGroup.blocksRaycasts = false;
        
        //Quickly Turn off UI
        float holder = fadeTime;
        fadeTime = 0;
        CloseVRUI();
        fadeTime = holder;
    }

	// Update is called once per frame
	void Update ()
    {
        Ray ray = new Ray(yourEyes.transform.position, yourEyes.forward);
        if (oneTapOpens)
        {
            if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Remote) || Input.GetKeyDown(KeyCode.Space))
            {
                if(!UIOn)
                {
                    OpenVRUI();
                }

            }
        }
        if(useDoubleTap)
        {
            if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Remote) || Input.GetKeyDown(KeyCode.Space))
            {
                if (tapCount == 1)
                {
                    if (tapTime > Time.time)
                    {
                        ToggleVRUI();
                    }
                    else
                    {
                        tapCount = 0;
                    }
                }
                else
                {
                    tapCount = 1;
                    tapTime = Time.time + tapSpeed;
                }

            }
            if (tapTime < Time.time)
            {
                tapCount = 0;
            }
            if (OVRInput.GetDown(OVRInput.Button.Back, OVRInput.Controller.Remote))
            {
                ToggleVRUI();
            }
        }
        if(Input.GetKeyDown(DebugKey))
        {
            ToggleVRUI();
        }
    }


    public void ToggleVRUI()
    {
        if (UIOn)
        {
            CloseVRUI();
        }
        else
        {
            OpenVRUI();
        }
    }

    public void CloseVRUI()
    {
        if(menuImage)
        {
            menuImage.sprite = menuSprite;
        }
        //Turn Off
        //RFlightControlsB.canMove = true;
        vrUIGroup.DOFade(0, fadeTime);
        vrUIGroup.blocksRaycasts = false;
        UIOn = false;
    }

    public void OpenVRUI()
    {
        if (menuImage)
        {
            menuImage.sprite = closeMenuSprite;
        }
        //Turn On
        //VRFlightControlsB.canMove = false;
        RepositionUI();
        vrUIGroup.DOFade(1, fadeTime);
        vrUIGroup.blocksRaycasts = true;
        UIOn = true;
    }

    void RepositionUI()
    {
        if(doNotReposition)
        {
            return;
        }
        Ray ray = new Ray(yourEyes.transform.position+Offset, yourEyes.forward);
        vrUIGroup.transform.position = ray.GetPoint(distance);
        vrUIGroup.transform.rotation = yourEyes.rotation;
        Vector3 holder = vrUIGroup.transform.rotation.eulerAngles;
        //Flattens UI Makes it always in the right place
        holder.z = 0;
        if(xOverRide)
        {
            holder.x = xOverRideNum;
        }
        vrUIGroup.transform.rotation = Quaternion.Euler(holder);
    }

}
