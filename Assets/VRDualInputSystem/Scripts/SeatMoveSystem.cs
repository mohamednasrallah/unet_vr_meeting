﻿using UnityEngine;
using System.Collections;

public class SeatMoveSystem : MonoBehaviour {

    public Vector3 seatOffset;
    public float speed = 4;
    public Transform OVRSeat;


    public void MoveFowardBack(int dir)
    {
        //x
        seatOffset.x += dir * (speed * Time.deltaTime);
        Vector3 holder = Vector3.right * (dir * speed * Time.deltaTime);
        PositionChanged(holder);
    }

    public void MoveUpDown(int dir)
    {
        //y
        seatOffset.y += dir * (speed * Time.deltaTime);
        Vector3 holder = Vector3.up * (dir * speed * Time.deltaTime);
        PositionChanged(holder);
    }

    public void MoveLeftRight(int dir)
    {
        //z
        seatOffset.z += dir * (speed * Time.deltaTime);
        Vector3 holder = Vector3.forward * (dir * speed * Time.deltaTime);
        PositionChanged(holder);
    }

    void PositionChanged(Vector3 chg)
    {
        OVRSeat.transform.position += chg;
        //Set Offset to the your Teleport MAnager
    }

}
