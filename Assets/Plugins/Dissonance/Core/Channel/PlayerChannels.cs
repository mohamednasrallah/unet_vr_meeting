﻿using Dissonance.Audio.Capture;

namespace Dissonance
{
    /// <summary>
    /// A collection of channels to players
    /// </summary>
    public sealed class PlayerChannels
        : Channels<PlayerChannel, string>
    {
        internal PlayerChannels(IChannelPriorityProvider priorityProvider)
            : base(priorityProvider)
        {

        }

        protected override PlayerChannel CreateChannel(ushort subscriptionId, string channelId, ChannelProperties properties)
        {
            return new PlayerChannel(subscriptionId, channelId, this, properties);
        }
    }
}
