﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplfiedMenuSystem : MonoBehaviour {

    GameObject yourMenu;
    public bool isOn;
    public OVRInput.Button button1;
    public OVRInput.Button button2;
    public OVRInput.Button button3;

    public void Update()
    {
        if(!yourMenu)
        {
            return;
        }
        if (OVRInput.GetDown(button1, OVRInput.Controller.LTouch))
        {
            ToggleMenu();
        }
        if (OVRInput.GetDown(button2, OVRInput.Controller.LTouch))
        {
            ToggleMenu();
        }
        if (OVRInput.GetDown(button3, OVRInput.Controller.LTouch))
        {
            ToggleMenu();
        }
    }

    public void GetUISystem(GameObject chg)
    {
        yourMenu = chg;
        Invoke("TurnOffSlowly",.2f);
        isOn = false;
    }
    public void TurnOffSlowly()
    {
        yourMenu.SetActive(false);
    }

    void ToggleMenu()
    {
        if(isOn)
        {
            yourMenu.SetActive(false);
            isOn = false;
        }
        else
        {
            yourMenu.SetActive(true);
            isOn = true;
        }
    }
}
