﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetFinderGameObject : MonoBehaviour {

    public static NetFinderGameObject instance;
    public GameObject theUI;

	// Use this for initialization
	void Start () {
        instance = this;
        theUI.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
